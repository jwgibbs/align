INSTALL
=======

Use the standard Python setup routine:

    python setup.py build
    python setup.py install

Explicitly set the C compiler with the CC environment variable before compiling:

    export CC=/usr/local/bin/gcc-4.8

Requires the following packages:

+ numpy
+ cython
+ scikit-image (skimage)

Some of the examples use image-3D (www.bitbucket.org/jwgibbs/im3d) but it's not necessary for general use. 

GENERAL
=======

This alignment routine is defined by three customizable functions:

1. A *transform* function that transforms (e.g. translates, rotates, resizes, ...) one dataset with respect to the other.
2. A *matching* function that determines how well the first dataset matches the second dataset.
3. A *minimize* function that tries to find the set of transformation parameters that gives the best match of the two datasets.

The general idea is that two datasets are loaded and one is designated as the fixed, reference dataset and the other is the transformable dataset. The *minimize* function generates some sets of transformation parameters. The transformable dataset is then altered by these amounts using the *transform* function. After each transformation amount, the *matching* function is used to determine how well the two datasets are aligned. The results will be given back to the *minimize* function, which will pick the transformation parameters that minimize the *matching* function.

As an example, imagine two 1D datasets:

    Dataset 1: 1, 2, 3, 4, 5, 6
    Dataset 2: 2, 3, 4, 5, 6, 7

A *transform* function could be defined that would shift the second dataset to the left or right relative to the first dataset, the *matching* dataset could be the mean of the differences between the two, and the *minimize* function could be trial and error. A reasonable set of parameters to try could be (-2, -1, 0, +1, +2) pixels, which would compare the datasets as follows, with the resulting values of the *matching* function listed on the right:

    Dataset 1:      1, 2, 3, 4, 5, 6
    
    Shifted by -2:  4, 5, 6, 7, -, -  -->  3  (= |+12|/4)
    Shifted by -1:  3, 4, 5, 6, 7, -  -->  2  (= |+10|/5)
    Shifted by +0:  2, 3, 4, 5, 6, 7  -->  1  (=  |+6|/6)
    Shifted by +1:  -, 2, 3, 4, 5, 6  -->  0  (=  |+0|/5)
    Shifted by +2:  -, -, 2, 3, 4, 5  -->  1  (=  |-4|/4)

Since the minimum value of the *matching* function occurs at a shift of +1, that would be selected as the best match.


Function details - matching
---------------------------

This package comes with several default functions but it has also been designed so that it is easy to replace one of the defaults with a custom function. The *matching* function default is mutual information [1]. If an alternative *matching* function is written, it is specified with the `matching_func` property, e.g.:

    register = align.align()
    register.matching_func = custom_matching_func

The custom *matching* function must have two positional arguments for the two datasets that are to be compared. Any additional parameters must be passed using keyword arguments in the `matching_func_param` dictionary, e.g.:

    def custom_matching_func(data1, data2, my_kwarg=None):
        ...
    
    register.matching_func = custom_matching_func
    register.matchign_func_param['my_kwarg'] = 4.0


Function details - transform
----------------------------

The *transform* function is similarly defined to the *matching* function; it must have two inputs: 1) an array to transform and 2) a list or tuple that contains the transformation parameters with any additional arguments begin specified with keyword arguments. For example, the default function of `align.transform.affine` has two arguments of an array and a tuple/list that contains exactly six inputs. The contents of the list/tuple are unpacked and used as the translation and rotation amounts.

The *transform* function and additional arguments are defined with:

    register = align.align()
    register.transform_func = custom_transform_func
    register.transform_func_param['my_kwarg'] = ...

Available built-in *transform* function are:

+ `align.transform.affine`
+ `align.transform.translate`
+ `align.transform.rotate`
+ `align.transform.Ztrans_Zrot`


Function details - minimize
---------------------------

The *minimize* function is the most complex of the three user definable functions because its behavior depends on the other functions. The requirements of this function are consistent with minimizers like the `scipy.optimize.fmin*` functions. The main requirements is for the first argument to be a function (the actual function that is passed to it is generated here in `align.__align_class._func_to_minimize`). A simple example is shown below:

    data1 = load_data(...)
    data2 = load_data(...)

    def func(trans):
        data_transformed = transform(data2, trans)
        matching = sum(abs(data_transformed - data1))
        return matching

    def fmin(func):
        values = (-1, 0, +1)
        for val in values:
            match = func(val)
        best_val = argmin(match)

[1] P. Viola and W. M. Wells III, International Journal of Computer Vision, vol. 24, no. 2, pp. 137–154, doi 10.1023/a:1007958904918 (1997)

