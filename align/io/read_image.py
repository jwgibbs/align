def read(fn):
    """
    USEAGE
    ======
    >>> fn = '/path/to/tiff_image.png'
    >>> im = read(fn)
    >>> ... do stuff with im ...
    
    INPUTS
    ======
    fn : string; required
        Filename of the image that you want to read
    
    OUTPUTS
    =======
    im : numpy array
        The image, formatted as an array
    
    NOTES
    =====
    This requires Scikits-image (aka skimage) and freeimage to be installed.
    
    """
    # Test to see if the file exists:
    import os
    if not os.path.exists(fn):
        raise IOError("File '%s' does not exist" % fn)
    # Load the necessary module
    import skimage.io
    skimage.io.use_plugin('freeimage')
    im = skimage.io.imread(fn, plugin='freeimage')
    # Return the array:
    return im
