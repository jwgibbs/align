import numpy as np
import os
import skimage.io
skimage.io.use_plugin('freeimage')

def write(fn, im, vmin=None, vmax=None, nbits=8):
    """
    USEAGE
    ======
    >>> 
    >>> 
    >>> 
    
    INPUTS
    ======
    fn : string; required
        Filename of the image that you want to read
    
    OUTPUTS
    =======
    im : numpy array
        The image, formatted as an array
    
    NOTES
    =====
    This requires Scikit-image (aka skimage) and freeimage to be installed.
    
    """
    # Test to see if the file exists:
    if not os.path.exists(os.path.dirname(fn)):
        raise IOError("File '%s' does not exist" % os.path.dirname(fn))
    # Scale the image to either 8 or 16 bits
    scaled_im = np.clip((im - vmin) / (vmax - vmin), 0.0, 1.0)
    if nbits == 16:
        scaled_im = scaled_im * (2**16 - 1)
        scaled_im = scaled_im.astype('uint16')
        #scaled_im = skimage.img_as_uint(scaled_im)
    elif nbits == 8:
        scaled_im = scaled_im * (2**8 - 1)
        scaled_im = scaled_im.astype('uint8')
        #scaled_im = skimage.img_as_ubyte(scaled_im)
    else:
        print("Can't use nbits=%i. Using nbits=8 instead" % nbits)
        scaled_im = scaled_im * (2**8 - 1)
        scaled_im = scaled_im.astype('uint8')
        #scaled_im = skimage.img_as_ubyte(scaled_im)
    # Save the image
    skimage.io.imsave(fn, scaled_im, plugin='freeimage')

