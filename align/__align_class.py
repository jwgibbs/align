class align(object):
    
    """
    A class that is used to align two 3D datasets.
    
    It is your responsibility to make sure all the functions are passed the right arguments. The expected function formats are:
        + align.align.matching_func(arr1, arr2)
            - arr1 -> np.array
            - arr2 -> np.array
            - The two arrays should have the same dimensions
        + align.align.minimize_func(TF, TF_args, any_other_minimization_args)
            - TF -> function that does the transformations
            - TF_args -> a list of args that are passed to TF as:
                TF(arr, transform_parameters, *TF_args)
        + align.align.transform_func(arr, amount, *args)
            - arr -> np.array
            - amount -> list, tuple or 1D array
            - args -> tuple
    
    Example usage:
        >>> ...
    """
    
    def __init__(self):
        import align.matching
        import align.minimize
        import align.transform
        # 
        self.__data_fixed = None
        self.__data_trans = None
        self.matching_func = align.matching.mutual_info
        self.minimize_func = align.minimize.quadratic_fit
        self.transform_func = align.transform.affine
        # 
        self.matching_func_param = {}
        self.minimize_func_param = {}
        self.transform_func_param = {}

    def _get_fixed(self):
        return self.__data_fixed
    
    def _get_trans(self):
        return self.__data_trans
    
    def _set_fixed(self, arr):
        import numpy as np
        if arr.dtype != 'float32':
            print('  Converting the data to 32-bit floats')
        self.__data_fixed = np.require(arr, dtype='float32', requirements=['C_contiguous',])
        if (self.__data_fixed != None and self.__data_trans != None):
            if self.__data_fixed.shape != self.__data_trans.shape:
                print("The 'fixed' and 'trans' datasets are different sizes! This will probably end badly...")
    
    def _set_trans(self, arr):
        import numpy as np
        if arr.dtype != 'float32':
            print('  Converting the data to 32-bit floats')
        self.__data_trans = np.require(arr, dtype='float32', requirements=['C_contiguous',])
        if (self.__data_fixed != None and self.__data_trans != None):
            if self.__data_fixed.shape != self.__data_trans.shape:
                print("The 'fixed' and 'trans' datasets are different sizes! This will probably end badly...")
    
    def _del_fixed(self):
        self.__data_fixed = None
    
    def _del_trans(self):
        self.__data_trans = None
    
    data_fixed = property(_get_fixed, _set_fixed, _del_fixed, 
        'Fixed array that is used as the reference.')
    
    data_trans = property(_get_trans, _set_trans, _del_trans, 
        'Translated array that is aligned to the reference array.')
    
    def _func_to_minimize(self, trans):
        """
        This function takes the transformation parameters that the minimizer 
        wants to try, applies them to the data and gets the resulting value
        from matching_func.
        """
        transformed = self.transform_func(self.__data_trans, trans)
        return self.matching_func(self.__data_fixed, transformed, **self.matching_func_param)
    
    def align(self):
        """
        fmin_param is a dictionary that has keys for the minimization scheme's kwargs
        """
        #--- check inputs ---
        if self.__data_fixed == None:
            raise InputError('data_fixed must have a value')
        if self.__data_trans == None:
            raise InputError('data_trans must have a value')
        #--- run minimizer ---
        print(self.minimize_func_param)
        self.final = self.minimize_func(self._func_to_minimize, **self.minimize_func_param)
