from __future__ import absolute_import

import numpy as np
from libc.math cimport log, floor
# ==============================================================
cdef inline int not_nan(float val):
    return 1 if val == val else 0

# ==============================================================
def mutual_info(arr1, arr2, bin_min=None, bin_max=None, nbins=256):
    cdef ssize_t  i, j, bin1, bin2, n=arr1.size, nB=nbins
    cdef double   MI, arr_sz
    cdef double   lo, hi, sz
    #
    cdef float[::1] flat1 = np.ravel(arr1).astype('float32')
    cdef float[::1] flat2 = np.ravel(arr2).astype('float32')
    #
    cdef double[:] H1 = np.zeros((nB), dtype='float64')
    cdef double[:] H2 = np.zeros((nB), dtype='float64')
    cdef double[:,:] HJ = np.zeros((nB, nB), dtype='float64')
    #=== Do some sanity checks ===
    if arr1.shape != arr2.shape:
        raise ValueError('The two arrays must be the same shape')
    #=== Setup stuff ===
    MI=0.0
    arr_sz=0.0
    
    if bin_min==None: lo = min(arr1.min(), arr2.min())
    else:             lo = bin_min
    
    if bin_max==None: hi = max(arr1.max(), arr2.max())
    else:             hi = bin_max
    
    sz=(hi-lo)/<double>(nB-1)
    #=== Calculate the histograms ===
    for i in range(n):
        if (not_nan(flat1[i]) and not_nan(flat2[i])):
            arr_sz += 1.0
            bin1 = <ssize_t> floor( (flat1[i] - lo) / sz + 0.5)
            bin2 = <ssize_t> floor( (flat2[i] - lo) / sz + 0.5)
            # 
            if ((bin1 >= 0) and (bin1 < nB) and (bin2 >= 0) and (bin2 < nB)):
                H1[bin1] += 1.0
                H2[bin2] += 1.0
                HJ[bin1,bin2] += 1.0
    #=== Calculate the mutual information ===
    for i in range(nB):
        if H1[i] > 0.0:
            MI += -(H1[i]/arr_sz) * log(H1[i]/arr_sz)
        if H2[i] > 0.0:
            MI += -(H2[i]/arr_sz) * log(H2[i]/arr_sz)
        for j in range(nB):
            if HJ[i,j] > 0.0:
                MI += (HJ[i,j]/arr_sz) * log(HJ[i,j]/arr_sz)
    #
    return -MI
