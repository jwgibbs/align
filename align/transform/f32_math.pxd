cdef extern from "math.h":
    float sqrtf(float) nogil
    float fabsf(float) nogil
    float sinf(float) nogil
    float cosf(float) nogil
    float atan2f(float, float) nogil
    float floorf(float) nogil
    float ceilf(float) nogil
    float logf(float) nogil
