#cython: boundscheck=False
#cython: wraparound=False
#cython: cdivision=True
#cython: embedsignature=True

import numpy as np

from f32_math cimport sinf, cosf, sqrtf, fabsf, atan2f, floorf, ceilf
from cython.parallel cimport prange
#=== translate function ========================================================
def translate(float[:,:,::1] in_arr, float dx, float dy, float dz):
    """
    dX, dY, dZ --> Amount of translation
    """
    #=== assign cython datatypes ===============================================
    cdef:
        ssize_t  x_out, nx=in_arr.shape[0]
        ssize_t  y_out, ny=in_arr.shape[1]
        ssize_t  z_out, nz=in_arr.shape[2]
        ssize_t  x_lo, y_lo, z_lo       # 
        ssize_t  x_hi, y_hi, z_hi       # 
        float   x_org, y_org, z_org    # Estimates of where (x,y) in
                                        # the out-array corresponds to
                                        # in the input array
        float   x_ctr, y_ctr, z_ctr    # Estimates of where (x,y) in
        float   x_rad, y_rad, z_rad    # radii for (x,y,z) in polar coord
        float   x_phi, y_phi, z_phi    # angles for (x,y,z) in polar coord
        float   Wx_lo, Wy_lo, Wz_lo
        float   Wx_hi, Wy_hi, Wz_hi
        float   W_LLL, W_LLH, W_LHL, W_LHH, W_HLL, W_HLH, W_HHL, W_HHH
        float[:,:,::1] arr = np.zeros((nx,ny,nz), dtype=np.float32)
    #=== do the translation ====================================================
    x_ctr = <float> nx / 2.0
    y_ctr = <float> ny / 2.0
    z_ctr = <float> nz / 2.0
    with nogil:
      for x_out in prange(nx):
        for y_out in range(ny):
          for z_out in range(nz):
            #--- Translate -----------------------------------------------------
            x_org = x_org - dx
            y_org = y_org - dy
            z_org = z_org - dz
            #--- Interpolate ---------------------------------------------------
            x_lo = <ssize_t> floorf(x_org)
            y_lo = <ssize_t> floorf(y_org)
            z_lo = <ssize_t> floorf(z_org)
            x_hi = x_lo + 1
            y_hi = y_lo + 1
            z_hi = z_lo + 1
            # 1D interpolation weights
            Wx_lo = - x_org + <float> x_hi
            Wy_lo = - y_org + <float> y_hi
            Wz_lo = - z_org + <float> z_hi
            Wx_hi = + x_org - <float> x_lo
            Wy_hi = + y_org - <float> y_lo
            Wz_hi = + z_org - <float> z_lo
            # 3D interpolation weights
            W_LLL = Wx_lo * Wy_lo * Wz_lo
            W_LLH = Wx_lo * Wy_lo * Wz_hi
            W_LHL = Wx_lo * Wy_hi * Wz_lo
            W_LHH = Wx_lo * Wy_hi * Wz_hi
            W_HLL = Wx_hi * Wy_lo * Wz_lo
            W_HLH = Wx_hi * Wy_lo * Wz_hi
            W_HHL = Wx_hi * Wy_hi * Wz_lo
            W_HHH = Wx_hi * Wy_hi * Wz_hi
            #
            if ((x_lo >= 0) and (x_hi < nx) and 
                (y_lo >= 0) and (y_hi < ny) and
                (z_lo >= 0) and (z_hi < nz)):
                arr[x_out,y_out,z_out] = W_LLL * in_arr[x_lo,y_lo,z_lo] + \
                             W_LLH * in_arr[x_lo,y_lo,z_hi] + \
                             W_LHL * in_arr[x_lo,y_hi,z_lo] + \
                             W_LHH * in_arr[x_lo,y_hi,z_hi] + \
                             W_HLL * in_arr[x_hi,y_lo,z_lo] + \
                             W_HLH * in_arr[x_hi,y_lo,z_hi] + \
                             W_HHL * in_arr[x_hi,y_hi,z_lo] + \
                             W_HHH * in_arr[x_hi,y_hi,z_hi]
          # end for z loop
        # end for y loop
      # end for x loop
    # end nogil
    return np.asarray(arr, dtype=np.float32)
