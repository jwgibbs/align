from align.transform.f32_affine import affine as _affine
from align.transform.f32_Ztrans_Zrot import _Ztrans_Zrot

def affine(arr, trans):
    import numpy as np
    # 
    arr = np.require(arr, dtype='float32', requirements=['C_contiguous',])
    dx, dy, dz, rx, ry, rz = trans
    return _affine(arr, dx, dy, dz, rx, ry, rz)

def Ztrans_Zrot(arr, trans):
    import numpy as np
    # 
    arr = np.require(arr, dtype='float32', requirements=['C_contiguous',])
    dz, rz = trans
    return _Ztrans_Zrot(arr, dz, rz)
