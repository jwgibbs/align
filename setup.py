from extension_builder import Package
from distutils.core import setup

pkg = Package('align', use_cython=True)

pkg.extra_compile_args.append("-Wno-maybe-uninitialized")
pkg.extra_compile_args.append("-O3")
pkg.extra_compile_args.append("-fopenmp")
pkg.extra_link_args.append("-fopenmp")

pkg.packages.append("align.matching")
pkg.packages.append("align.minimize")
pkg.packages.append("align.transform")
pkg.packages.append("align.io")

pkg.make_setup()
setup(**pkg.setup)
