#=== create datasets ===========================================================
import im3D
import numpy as np

data_ref = np.tanh(im3D.shapes.sphere(80, 24)) + np.random.uniform(low=-0.10, high=+0.10, size=(80,80,80))
data_alt = np.tanh(im3D.shapes.sphere(80, 24)) + np.random.uniform(low=-0.10, high=+0.10, size=(80,80,80))

dx = +6
dy = +3
dz = -7

data_ref = data_ref[8-dx:80-8-dx, 8-dy:80-8-dy, 8-dz:80-8-dz]
data_alt = data_alt[8:80-8, 8:80-8, 8:80-8]
#=== required setup ============================================================
import align

register = align.align()

register.data_fixed = data_ref
register.data_trans = data_alt

register.minimize_func_param['initial'] = (0, 0, 0, 0, 0, 0)
#=== optional setup ============================================================
register.transform_func = align.transform.affine
register.matching_func = align.matching.mutual_info
register.minimize_func = align.minimize.quadratic_fit

register.matching_func_param['nbins'] = 256

register.minimize_func_param['verbose'] = False
register.minimize_func_param['order'] = [0, 1, 2]
register.minimize_func_param['iterations'] = 4
register.minimize_func_param['scale'] = (1.0, 1.0, 1.0, 1.0, 1.0, 1.0)
register.minimize_func_param['nvals'] = (5, 5, 5, 5, 5, 5)
register.minimize_func_param['fun_tol'] = (0.01, 0.01, 0.01, 0.01, 0.01, 0.01)
register.minimize_func_param['val_tol'] = (0.01, 0.01, 0.01, 0.01, 0.01, 0.01)
#=== run alignment =============================================================
register.align()

print('dx = %.2f' % register.final[0])
print('dy = %.2f' % register.final[1])
print('dz = %.2f' % register.final[2])
#=== show the results ==========================================================
import matplotlib.pyplot as plot

data_reg = register.transform_func(data_alt, register.final)
plot.imshow(data_reg[:,24,:])
plot.contour(data_ref[:,24,:], levels=[0], colors='r')
plot.contour(data_reg[:,24,:], levels=[0], colors='b')
