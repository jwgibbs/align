import numpy as np
import im3D
import glob
import os
import align.io

def load_data(fn_regex):
    """
    inputs
    ------
        fn_regex : string
            Regular expression for the files
    
    output
    ------
        data : numpy.array
            An array containing the image data
    
    example
    -------
        >>> fn_regex = 'image_????.tiff'
        >>> data_arr = load_data(fn_regex)
    
    notes
    -----
    
    """
    print('reading files from: %s' % os.path.dirname(fn_regex))
    fn_list = glob.glob(fn_regex)
    fn_list.sort()
    test_im = align.io.read(fn_list[0])
    nx, ny = test_im.shape
    arr = np.zeros((nx, ny, len(fn_list)), dtype='float32')
    for i in range(len(fn_list)):
        fn = fn_list[i]
        print('    %4i/%4i: %s' % (i+1, len(fn_list), os.path.basename(fn)))
        arr[:,:,i] = align.io.read(fn)
    print('')
    return arr

def downsample(arr):
    nx = int(np.floor(arr.shape[0] / 2))
    ny = int(np.floor(arr.shape[1] / 2))
    nz = int(np.floor(arr.shape[2] / 2))
    small = np.empty((nx, ny, nz), dtype=np.float32)
    small[...] = (arr[0:2*nx:2, 0:2*ny:2, 0:2*nz:2] + 
                  arr[1:2*nx:2, 0:2*ny:2, 0:2*nz:2] + 
                  arr[0:2*nx:2, 1:2*ny:2, 0:2*nz:2] + 
                  arr[1:2*nx:2, 1:2*ny:2, 0:2*nz:2] + 
                  arr[0:2*nx:2, 0:2*ny:2, 1:2*nz:2] + 
                  arr[1:2*nx:2, 0:2*ny:2, 1:2*nz:2] + 
                  arr[0:2*nx:2, 1:2*ny:2, 1:2*nz:2] + 
                  arr[1:2*nx:2, 1:2*ny:2, 1:2*nz:2]) / 8.0
    return small

