import sys, os, stat, site
from distutils.core import setup
from distutils.extension import Extension

class Package:
    
    def __init__(self, src_dir='.', use_cython=False, packages=None):
        """
        ...
        """
        self.src_dir = src_dir
        self.use_cython = use_cython
        self.include_dirs = [site.PREFIXES[0]+'/include', '.']   # adding the '.' to include_dirs is CRUCIAL!!
        self.library_dirs = [site.PREFIXES[0]+'/lib']
        self.extra_compile_args = []
        self.extra_link_args = []
        self.libraries = ['python2.7',]
        self.CC = None
        if packages == None:
                self.packages = [self.src_dir,]
        #=== Check for Cython ===
        if self.use_cython == True:
            try:
                from Cython.Distutils import build_ext
                self.build_ext = build_ext
            except:
                #raise ImportError('Cython is required for this install')
                print('Cython is not available. Using existing .c files')
                self.use_cython = False
        #=== Discover modules ===
        self.modules = self._find_module_names(self.src_dir)
    
    
    def _find_module_names(self, dir, modules=[]):
        """
        Scan a directory for .pyx files, converting the file structure to 
        extension names (i.e. dotted names)
        
        inputs
        ======
            dir : string, required
                The directory that you want to scan
            modules : list, optional
                A list of the modules in dir. Can be added to, e.g.:
                >>> some_modules = find_module_names('dir_1')
                >>> more_modules = find_module_names('dir_2', modules=some_modules)
        """
        import os
        for file in os.listdir(dir):
            filepath = os.path.join(dir, file)
            if os.path.isfile(filepath) and filepath.endswith(".pyx"):
                module_name = filepath.replace(os.path.sep, ".")[:-4]
                modules.append(module_name)
            elif os.path.isdir(filepath):
                self._find_module_names(filepath, modules)
        return modules
    
    
    def _build_ext(self, ext_name):
        ext_path = ext_name.replace(".", os.path.sep)+".pyx"
        return Extension(
            ext_name,
            [ext_path],
            include_dirs = self.include_dirs,
            library_dirs = self.library_dirs,
            extra_compile_args = self.extra_compile_args,
            extra_link_args = self.extra_link_args,
            libraries = self.libraries,
            )
    
    
    def make_setup(self, version=None, author=None, author_email=None, license=None, 
        description=None, long_description=None, url=None):
        """
        
        """
        print('Extension compile flags:')
        print('  include dirs: %s' % self.include_dirs)
        print('  library_dirs: %s' % self.library_dirs)
        print('  extra_compile_args: %s' % self.extra_compile_args)
        print('  extra_link_args: %s' % self.extra_link_args)
        print('  libraries: %s' % self.libraries)
        
        self.extensions = [self._build_ext(module) for module in self.modules]
        
        self.setup = dict(name=self.src_dir,
            version = version,
            author = author, 
            author_email = author_email,
            packages = self.packages,
            url = url,
            license = license,
            description = description,
            long_description = long_description,
            ext_modules = self.extensions,
            cmdclass = {'build_ext': self.build_ext})


if __name__ == '__main__':
    #from extension_builder import Package
    from distutils.core import setup
    # 
    pkg = Package('im3D', use_cython=True)
    # Add some arguments to the extension builder
    #import numpy as np
    #pkg.include_dirs.append(np.get_include())
    pkg.extra_compile_args.append("-Wno-maybe-uninitialized")
    pkg.extra_compile_args.append("-O3")
    pkg.extra_compile_args.append("-fopenmp")
    pkg.extra_link_args.append("-fopenmp")
    # 
    pkg.make_setup()
    setup(**pkg.setup)
    